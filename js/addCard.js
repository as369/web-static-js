
let hpValue = 0;
let energyValue = 0;
let attackValue = 0;
let defenceValue = 0;

function setHpValue(value) {
    hpValue = value;
}

function setEnergyValue(value) {
    energyValue = value;
}

function setAttackValue(value) {
    attackValue = value;
}

function setDefenceValue(value) {
    defenceValue = value;
}

$("#hpSlider").on("input change", (e) => { 
    setHpValue(e.target.value);
});

$("#energySlider").on("input change", (e) => { 
    setEnergyValue(e.target.value);
});

$("#attackSlider").on("input change", (e) => { 
    setAttackValue(e.target.value);
});

$("#defenceSlider").on("input change", (e) => { 
    setDefenceValue(e.target.value);
});

function getInputValue(idInput) {
    return $(`#${idInput}`).val();
}

function getSelectValue(idSelect) {
    return $(`#${idSelect} option:selected`).text();
}

function createCard () {
    const cardName = getInputValue('nameInput');
    const cardDescription = getInputValue('descriptionInput');
    const cardImageUrl = getInputValue('imageUrlInput');
    const cardFamily = getSelectValue('familySelect');
    const cardAffinity = getSelectValue('affinitySelect');

    return {
        name: cardName,
        description: cardDescription,
        family: cardFamily,
        affinity: cardAffinity,
        imgUrl: cardImageUrl,
        smallImgUrl: "https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/05.jpg",
        energy: energyValue,
        hp: hpValue,
        defence: defenceValue,
        attack: attackValue,
        price: 0.0,
        userId: null
    };
}

async function add() {

    const card = createCard();

    try {
        await axios.post(
            'https://asi2-backend-market.herokuapp.com/card',
            card,
            {
                headers: {
                    'Content-Type': 'application/json'
                }
        });
        $('#notification').removeClass("hide");
        $('#notification').addClass("display");
    } catch (errors) {
        console.error(errors);
    }
} 

$("#submitCard").click(async (e) => {
    e.preventDefault();
    add();
    window.history.pushState('page2', 'Title', '/atelier1/views/addCard.html');
});
