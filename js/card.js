let cardlist = [
        {
            family_src:"https://imgc.allpostersimages.com/img/print/affiches/marvel-super-hero-squad-iron-man-standing_a-G-9448041-4985690.jpg",
            family_name:"Jose",
            image_src:"https://imgc.allpostersimages.com/img/print/affiches/marvel-super-hero-squad-iron-man-standing_a-G-9448041-4985690.jpg",
            date:"14h",
            comment:"3 comments",
            like: "17 likes",
            button: "Buy"
        },
        {
            family_src:"https://media.giphy.com/media/l4q8hciiYNT5RGi4w/giphy.gif",
            family_name:"John",
            image_src:"https://media.giphy.com/media/l4q8hciiYNT5RGi4w/giphy.gif",
            date:"1h",
            comment:"345 comments",
            like: "1000 likes",
            button: "Read"
        },
        {
            family_src:"http://www.superherobroadband.com/app/themes/superhero/assets/img/superhero.gif",
            family_name:"Elliot",
            image_src:"http://www.superherobroadband.com/app/themes/superhero/assets/img/superhero.gif",
            date:"142h",
            comment:"0 comment",
            like: "1 likes",
            button: "Read"
        }

    ];

async function getCards() {
    const data = await axios.get('https://asi2-backend-market.herokuapp.com/cards');
    return data.data;
}

async function populateTemplate() {
    let template = document.querySelector("#selectedCard");

    const regEx = /\b(https?:\/\/.*?\.[a-z]{2,4}\/[^\s]*\b)/g;

    const cards = await getCards();

    for(const card of cards){
        let clone = document.importNode(template.content, true);

        newContent = clone.firstElementChild.innerHTML
                    .replace(/{{smallLogo}}/g, _.isEmpty(card.smallImgUrl) || !card.smallImgUrl.match(regEx) ? 'http://medias.3dvf.com/news/sitegrab/gits2045.jpg' : card.smallImgUrl)
                    .replace(/{{cardName}}/g, _.isEmpty(card.name) ? 'Default' : card.name)
                    .replace(/{{cardImage}}/g, _.isEmpty(card.imgUrl) || !card.imgUrl.match(regEx) ? 'https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/01.jpg' : card.imgUrl)
                    .replace(/{{cardDescription}}/g, _.isEmpty(card.description) ? 'Default' : card.description)
                    .replace(/{{hp}}/g, _.isEmpty(card.hp) ? 50 : card.hp)
                    .replace(/{{energy}}/g, _.isEmpty(card.energy) ? 50 : card.energy)
                    .replace(/{{defence}}/g, _.isEmpty(card.defence) ? 50 : card.defence)
                    .replace(/{{attack}}/g, _.isEmpty(card.attack) ? 50 : card.attack);
        clone.firstElementChild.innerHTML = newContent;

        let cardContainer = document.querySelector("#gridContainer");
        cardContainer.appendChild(clone);
    }
}

populateTemplate();